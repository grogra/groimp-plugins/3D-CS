/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.arrangeBlock;

import de.grogra.persistence.SCOType;

public class FProbability extends ShareableProbabilityArrangeBase implements ProbabilityArrangeMethod
{
	//enh:sco SCOType
	
	int num_deg_freedom = 4;
	// enh:field getter setter min=0 max=50

	int den_deg_freedom = 5;
	// enh:field getter setter min=0 max=50
	
	public void calculate() {
		for (int i=0; i<probabilityValues.length; i++) {
			probabilityValues[i] = (int)Math.abs(Math.round(source.f(num_deg_freedom, den_deg_freedom)));
		}
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field num_deg_freedom$FIELD;
	public static final Type.Field den_deg_freedom$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (FProbability representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 2;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((FProbability) o).num_deg_freedom = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 1:
					((FProbability) o).den_deg_freedom = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((FProbability) o).getNum_deg_freedom ();
				case Type.SUPER_FIELD_COUNT + 1:
					return ((FProbability) o).getDen_deg_freedom ();
			}
			return super.getInt (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new FProbability ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (FProbability.class);
		num_deg_freedom$FIELD = Type._addManagedField ($TYPE, "num_deg_freedom", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 0);
		den_deg_freedom$FIELD = Type._addManagedField ($TYPE, "den_deg_freedom", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 1);
		num_deg_freedom$FIELD.setMinValue (new Integer (0));
		num_deg_freedom$FIELD.setMaxValue (new Integer (50));
		den_deg_freedom$FIELD.setMinValue (new Integer (0));
		den_deg_freedom$FIELD.setMaxValue (new Integer (50));
		$TYPE.validate ();
	}

	public int getNum_deg_freedom ()
	{
		return num_deg_freedom;
	}

	public void setNum_deg_freedom (int value)
	{
		this.num_deg_freedom = (int) value;
	}

	public int getDen_deg_freedom ()
	{
		return den_deg_freedom;
	}

	public void setDen_deg_freedom (int value)
	{
		this.den_deg_freedom = (int) value;
	}

//enh:end

}

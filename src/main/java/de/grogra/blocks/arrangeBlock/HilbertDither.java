/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;

public class HilbertDither extends ArrangeBase {

    private PointArrayList hilbertPointList = new PointArrayList();
    private Point oldP = new Point(0,0);
	
	private int lengthField = 128;

	private double error = 0.0;

	public HilbertDither(float maxX, float maxY, 
			float threshold, float maxThreshold, float[][] field) {
		final int FIELD_LENGTH = field.length;
		threshold *= maxThreshold;

		//die HilbertKurve erstellen und Punkte in hilbertPointList speichern
		makeHilbert();
		
		double old = 0; 
		int xi = 0, yi = 0;
		for (int i = 0; i < hilbertPointList.size(); i++) {
			Point p = hilbertPointList.get(i);
			xi = (int)p.getX();
			yi = (int)p.getY();
			field[xi][yi] += error;
			old = field[xi][yi];
			if (field[xi][yi] < threshold) {
				field[xi][yi] = 0;
			} else {
				field[xi][yi] = maxThreshold;
			}
			error = Math.abs(old - field[xi][yi]);
		}		

		// berechneten Punkte uebertragen
		pointList = new PointArrayList();
		for (int i = 0; i < FIELD_LENGTH; i++) {
			for (int j = 0; j < FIELD_LENGTH; j++) {
				if (field[j][i] == maxThreshold) {
					pointList.add(new Point(i*(maxX-1)/FIELD_LENGTH, j*(maxY-1)/FIELD_LENGTH));	
				}
			}
		}
		
		pointListToArrays();
	}

    private void makeHilbert() {
        int level=7; // rekursionstiefe ==> 128 segmente
        for (int i=level; i>0; i--) lengthField /= 2;
        addPoint(lengthField/2,lengthField/2);       
        hilbertA(level);        
    }
    
    private void addPoint(float x, float y) {
    	Point p = new Point((int)(oldP.getX()+x), (int)(oldP.getY()+y));
    	oldP = p;
    	hilbertPointList.add(p);
    }

    private void hilbertA (int level) {
        if (level > 0) {
            hilbertB(level-1);
            addPoint(0,lengthField);
            hilbertA(level-1);
            addPoint(lengthField,0);
            hilbertA(level-1);    
            addPoint(0,-lengthField);
            hilbertC(level-1);
        }
    }

    private void hilbertB (int level) {
        if (level > 0) {
            hilbertA(level-1);    
            addPoint(lengthField,0);
            hilbertB(level-1);
            addPoint(0,lengthField);
            hilbertB(level-1);
            addPoint(-lengthField,0);
            hilbertD(level-1);
        }
    }

    private void hilbertC (int level) {
        if (level > 0) {
            hilbertD(level-1);
            addPoint(-lengthField,0);
            hilbertC(level-1);
            addPoint(0,-lengthField);
            hilbertC(level-1);
            addPoint(lengthField,0);
            hilbertA(level-1);
        }
    }

    private void hilbertD (int level) {
        if (level > 0) {
            hilbertC(level-1);
            addPoint(0,-lengthField);            
            hilbertD(level-1);
            addPoint(-lengthField,0);
            hilbertD(level-1);
            addPoint(0,lengthField);
            hilbertB(level-1);
        }
    }
	
	
}

/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.PointArrayList;
import de.grogra.blocks.Arrange;
import de.grogra.persistence.ShareableBase;
import de.grogra.rgg.Library;

public abstract class ShareableAdditionalArrangeBase extends ShareableBase implements AdditionalArrangeMethod {

	protected final static int maxF = ArrangeBase.maxF;
	protected final static float prozent = ArrangeBase.prozent;

	protected final static int MAX_NUMBER = ShareableArrangeBase.MAX_NUMBER;
	
	protected float maxX; 
	protected float maxY;
	
	protected PointArrayList pointList = null;

	protected float[][] densityField = null;
	
	protected int superNumber = -1;
	
	public abstract void calculate();
	
	protected boolean checkDensityField(double xi, double yi) {
		int xii = (int)(xi*(densityField.length-1)/maxX);
		int yii = (int)(yi*(densityField.length-1)/maxY);
		if (xii==Arrange.MAX_RASTER) {
			xii--;
		}
		if (yii==Arrange.MAX_RASTER) {
			yii--;
		}		
		return Library.random(0,1) < densityField[xii][yii];
	}
	
	public PointArrayList setAll(float maxX, float maxY, float[][] densityField) {
		this.maxX = maxX - 1;
		this.maxY = maxY - 1;		

		this.densityField = densityField;

		calculate();
		return pointList;
	}

	public void setNumber(int number) {
		superNumber = number;
	}
	
}

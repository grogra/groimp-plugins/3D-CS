/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.arrangeBlock;

import raskob.geometry.Point;
import raskob.geometry.PointArrayList;

public class Jarvis12Dither extends ArrangeBase {
	
	public Jarvis12Dither(float maxX, float maxY, 
			float threshold, float maxThreshold, float[][] field) {
		final int FIELD_LENGTH = field.length;
		threshold *= maxThreshold;

		// in Punkte umrechnen (Halftoning mittels: Error-Diffusion-12 Nachbarn = Jarvis)
		final double a = 7.0 / 48.0;
		final double b = 5.0 / 48.0;
		final double c = 3.0 / 48.0;
		final double d = 5.0 / 48.0;
		final double e = 7.0 / 48.0;
		final double f = 5.0 / 48.0;
		final double g = 3.0 / 48.0;
		final double h = 1.0 / 48.0;
		final double i = 3.0 / 48.0;
		final double j = 5.0 / 48.0;
		final double k = 3.0 / 48.0;
		final double l = 1.0 / 48.0;
		double error = 0.0, old = 0;
		for (int ii = 0; ii < FIELD_LENGTH - 2; ii++) {
			for (int jj = 2; jj < FIELD_LENGTH - 2; jj++) {
				old = field[jj][ii];
				if (field[jj][ii] < threshold) {
					field[jj][ii] = 0;
				} else {
					field[jj][ii] = maxThreshold;
				}
				error = Math.abs(old - field[jj][ii]);
				
				field[jj + 1][ii] += error * a;
				field[jj + 2][ii] += error * b;
				
				field[jj - 2][ii + 1] += error * c;
				field[jj - 1][ii + 1] += error * d;
				field[jj]    [ii + 1] += error * e;
				field[jj + 1][ii + 1] += error * f;
				field[jj + 2][ii + 1] += error * g;
				
				field[jj - 2][ii + 2] += error * h;
				field[jj - 1][ii + 2] += error * i;
				field[jj]    [ii + 2] += error * j;
				field[jj + 1][ii + 2] += error * k;
				field[jj + 2][ii + 2] += error * l;				
			}
		}

		// berechneten Punkte uebertragen
		pointList = new PointArrayList();
		for (int ii = 0; ii < FIELD_LENGTH; ii++) {
			for (int jj = 0; jj < FIELD_LENGTH; jj++) {
				if (field[jj][ii] == maxThreshold) {
					pointList.add(new Point(ii*(maxX-1)/FIELD_LENGTH, jj*(maxY-1)/FIELD_LENGTH));
				}
			}
		}
		
		pointListToArrays();
	}

}
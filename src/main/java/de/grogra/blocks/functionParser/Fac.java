/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/**
 * Fac.java
 *
 *
 * @author:	Michael Henke
 * @date:	10.09.2006
 */
package de.grogra.blocks.functionParser;

public class Fac extends UnaryExpr {

	public Fac(Expr operand) {
		super(operand);
	}

	private long fac(int n) {
		long faku;
		if (n > 1) {
			faku = fac(n - 1) * n;
		} else {
			faku = 1;
		}
		return faku;
	}

	// eval wertet den Operanden aus
	public double eval() {
		return fac((int) operand.eval());
	}

	public String toString() {
		return "fac(" + operand.toString() + ")";
	}
}

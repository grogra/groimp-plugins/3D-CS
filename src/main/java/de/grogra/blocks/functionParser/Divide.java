/**
 * Divide.java
 *
 *
 * @author:	Michael Henke
 * @date:	25.10.2002
 */

package de.grogra.blocks.functionParser;

/*
 * eine Klasse für das Divide
 */
public class Divide extends BinaryExpr {

	public Divide(Expr left, Expr right) {
		super(left, right);
	}

	public double eval() {
		return left.eval() / right.eval();
	}

	public String toString() {
		return "(" + left.toString() + " / " + right.toString() + ")";
	}
}

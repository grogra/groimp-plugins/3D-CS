/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/**
 * UnaryExpr.java
 *
 *
 * @author:	Michael Henke
 * @date:	25.10.2002
 */
package de.grogra.blocks.functionParser;

/*
 * eine abstrakte Klasse für unäre Ausdrücke
 */
public abstract class UnaryExpr extends Expr {

	// der Operanden-Ausdruck
	protected Expr operand;

	protected UnaryExpr(Expr operand) {
		this.operand = operand;
	}

	public void setX(double x) {
		operand.setX(x);
	}

	public void setI(double x) {
		operand.setI(x);
	}
	
	public void setPreI(double x) {
		operand.setPreI(x);
	}

	public void setP(double x) {
		operand.setP(x);
	}

	public void setD(double x) {
		operand.setD(x);
	}

	public void setN1(double x) {
		operand.setN1(x);
	}

	public void setN2(double x) {
		operand.setN2(x);
	}

	public void setN3(double x) {
		operand.setN3(x);
	}

	public void setH(double x) {
		operand.setH(x);
	}

	public void setHl(double x) {
		operand.setHl(x);
	}

}

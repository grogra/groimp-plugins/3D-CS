
/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks;

import javax.vecmath.Vector3d;

import de.grogra.imp.View;
import de.grogra.imp3d.View3D;
import de.grogra.math.Id;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;
import de.grogra.xl.lang.FloatToFloat;

public class TreeLOD extends ShareableBase 
{

	//enh:sco SCOType
	
	boolean useLOD = true;
	// enh:field getter setter	

	int branchesNumber = 2*View.LOD_MAX;
	// enh:field getter setter min=View.LOD_MIN max=2*View.LOD_MAX

	FloatToFloat branchesNumberMode = new Id();
	// enh:field getter setter		
	
	int minBranchesNumber = 0;
	// enh:field getter setter min=0 max=50	
	
	int scale = View.LOD_MIN;
	// enh:field getter setter min=View.LOD_MIN max=2*View.LOD_MAX
	
	FloatToFloat scaleMode = new Id();
	// enh:field getter setter		
	
	boolean profile = true;
	// enh:field setter
	
	
	private final float MAX_PIXEL = 20;
	private float localLod_P = 1.0f;
	private float globalLod_P = 1.0f;

	
	public void set (View view, Vector3d v, float size)
	{
		View3D view3d = (view instanceof View3D) ? (View3D) view : null;		
		// global gesetzte lod wert
		int globalLod = (view3d != null) ? view3d.getViewComponent().getGlobalLOD() : View.LOD_MAX;
		// anzahl pixel, die aktuell benoetigt werden, um eine strecke der laenge 1 zu zeichnen
		float lengthOfOne = size * ((view3d != null) ? view3d.getCanvasCamera().getScaleAt(v.x, v.y, v.z) : MAX_PIXEL);
		// akt global in prozent von lod_max 
		globalLod_P = (globalLod+1)/(float)(View.LOD_MAX+1);
		// akt local length von 1 (in pixel) in prozent von max_pixel 
		localLod_P  = (lengthOfOne/MAX_PIXEL<1) ? lengthOfOne/MAX_PIXEL : 1.0f;
		// akt lod wert: local prozent vom globalen
//		aktLod_P = localLod_P * globalLod_P;
		
		if (View.LOD_MIN == globalLod) {
//			profile = false;
		}
		
//System.out.println (" lod (" + lengthOfOne+ " = " + localLod_P + ") =!= ("+globalLod+ ":"+globalLod_P+")  \t\t  15 = "+branches_NumberToLod(15));		
	}
	
	private float toAktLod(float value)
	{		
		return branchesNumberMode.evaluateFloat(localLod_P) * globalLod_P * value;
	}

	protected float branches_NumberToLod(float number)
	{
		if (useLOD) {
			float n = toAktLod((branchesNumber+1)/(float)(2*View.LOD_MAX+1) * number);		 
			if (n<minBranchesNumber) {
				return minBranchesNumber;
			}
			return n;
		}
		return number;
	}
	
	protected float scaleToLod(float number)
	{
		if (useLOD) {		
			return number + (1-(scaleMode.evaluateFloat(localLod_P) * globalLod_P))*((scale+1)/(float)(2*View.LOD_MAX+1));
		}
		return number;			
	}
	
	public boolean isProfile ()
	{
		return (useLOD)?profile:true;
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field useLOD$FIELD;
	public static final Type.Field branchesNumber$FIELD;
	public static final Type.Field branchesNumberMode$FIELD;
	public static final Type.Field minBranchesNumber$FIELD;
	public static final Type.Field scale$FIELD;
	public static final Type.Field scaleMode$FIELD;
	public static final Type.Field profile$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (TreeLOD representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 7;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setBoolean (Object o, int id, boolean value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((TreeLOD) o).useLOD = (boolean) value;
					return;
				case Type.SUPER_FIELD_COUNT + 6:
					((TreeLOD) o).profile = (boolean) value;
					return;
			}
			super.setBoolean (o, id, value);
		}

		@Override
		protected boolean getBoolean (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((TreeLOD) o).isUseLOD ();
				case Type.SUPER_FIELD_COUNT + 6:
					return ((TreeLOD) o).profile;
			}
			return super.getBoolean (o, id);
		}

		@Override
		protected void setInt (Object o, int id, int value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					((TreeLOD) o).branchesNumber = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 3:
					((TreeLOD) o).minBranchesNumber = (int) value;
					return;
				case Type.SUPER_FIELD_COUNT + 4:
					((TreeLOD) o).scale = (int) value;
					return;
			}
			super.setInt (o, id, value);
		}

		@Override
		protected int getInt (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 1:
					return ((TreeLOD) o).getBranchesNumber ();
				case Type.SUPER_FIELD_COUNT + 3:
					return ((TreeLOD) o).getMinBranchesNumber ();
				case Type.SUPER_FIELD_COUNT + 4:
					return ((TreeLOD) o).getScale ();
			}
			return super.getInt (o, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					((TreeLOD) o).branchesNumberMode = (FloatToFloat) value;
					return;
				case Type.SUPER_FIELD_COUNT + 5:
					((TreeLOD) o).scaleMode = (FloatToFloat) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 2:
					return ((TreeLOD) o).getBranchesNumberMode ();
				case Type.SUPER_FIELD_COUNT + 5:
					return ((TreeLOD) o).getScaleMode ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new TreeLOD ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (TreeLOD.class);
		useLOD$FIELD = Type._addManagedField ($TYPE, "useLOD", 0 | Type.Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, Type.SUPER_FIELD_COUNT + 0);
		branchesNumber$FIELD = Type._addManagedField ($TYPE, "branchesNumber", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 1);
		branchesNumberMode$FIELD = Type._addManagedField ($TYPE, "branchesNumberMode", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, Type.SUPER_FIELD_COUNT + 2);
		minBranchesNumber$FIELD = Type._addManagedField ($TYPE, "minBranchesNumber", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 3);
		scale$FIELD = Type._addManagedField ($TYPE, "scale", 0 | Type.Field.SCO, de.grogra.reflect.Type.INT, null, Type.SUPER_FIELD_COUNT + 4);
		scaleMode$FIELD = Type._addManagedField ($TYPE, "scaleMode", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, Type.SUPER_FIELD_COUNT + 5);
		profile$FIELD = Type._addManagedField ($TYPE, "profile", 0 | Type.Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, Type.SUPER_FIELD_COUNT + 6);
		branchesNumber$FIELD.setMinValue (new Integer (View.LOD_MIN));
		branchesNumber$FIELD.setMaxValue (new Integer (2*View.LOD_MAX));
		minBranchesNumber$FIELD.setMinValue (new Integer (0));
		minBranchesNumber$FIELD.setMaxValue (new Integer (50));
		scale$FIELD.setMinValue (new Integer (View.LOD_MIN));
		scale$FIELD.setMaxValue (new Integer (2*View.LOD_MAX));
		$TYPE.validate ();
	}

	public boolean isUseLOD ()
	{
		return useLOD;
	}

	public void setUseLOD (boolean value)
	{
		this.useLOD = (boolean) value;
	}

	public void setProfile (boolean value)
	{
		this.profile = (boolean) value;
	}

	public int getBranchesNumber ()
	{
		return branchesNumber;
	}

	public void setBranchesNumber (int value)
	{
		this.branchesNumber = (int) value;
	}

	public int getMinBranchesNumber ()
	{
		return minBranchesNumber;
	}

	public void setMinBranchesNumber (int value)
	{
		this.minBranchesNumber = (int) value;
	}

	public int getScale ()
	{
		return scale;
	}

	public void setScale (int value)
	{
		this.scale = (int) value;
	}

	public FloatToFloat getBranchesNumberMode ()
	{
		return branchesNumberMode;
	}

	public void setBranchesNumberMode (FloatToFloat value)
	{
		branchesNumberMode$FIELD.setObject (this, value);
	}

	public FloatToFloat getScaleMode ()
	{
		return scaleMode;
	}

	public void setScaleMode (FloatToFloat value)
	{
		scaleMode$FIELD.setObject (this, value);
	}

//enh:end
	
}

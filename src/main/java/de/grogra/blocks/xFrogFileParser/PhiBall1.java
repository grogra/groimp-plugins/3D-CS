/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import de.grogra.blocks.PhiBall;
import de.grogra.graph.impl.Node;

public class PhiBall1 extends Expr {

	private final String phiballImport = "import de.grogra.blocks.PhiBall;";

	private String name = "";

	public PhiBall1(Expr a, Expr b, Expr c, Expr d, Expr e, Expr f, Expr g) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
		this.g = g;

		if (aktKeyFrame == 0) {
			name = new String(aktStructName);
			blocks.put(name, toXL());
			blocksGraphNodes.put(name, toGraph());
			if (!imports.contains(phiballImport))
				imports.add(phiballImport);
			use_GFXColor = false;
			aktTexture = null;			
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	private String toXL() {
		PhiBall obj = new PhiBall();
		String ss = "";

		ss += "setName(" + name + "), ";

		// anz Segmente
		if (!((FLOAT) (a.a.a.a)).equalsF5(obj.getNumber()))
			ss += "setNumber(" + a.a.a.a + "), ";

		// radius
		if (!((FLOAT) (b.a.a.a)).equalsF5(obj.getRadiusX()))
			ss += "setRadius(" + b.a.a.a + "), ";

		// Fan
		if (!((FLOAT) (d.a.a)).equalsF5(obj.getFan2()))
			ss += "setFan2(" + d.a.a + "), ";
		if (!((FLOAT) (d.a.b)).equalsF5(obj.getFan1()))
			ss += "setFan1(" + d.a.b + "), ";
		if (!obj.getFanMode().getClass().getSimpleName().equals(
				((Functions) d.a.c).getFunction().getClass().getSimpleName()))
			ss += "setFanMode(" + ((Functions) d.a.c).toXL() + "), ";

		// angle
		if (!((FLOAT) (g.a.a)).equalsF5(obj.getAngle2()))
			ss += "setAngle2(" + g.a.a + "), ";
		if (!((FLOAT) (g.a.b)).equalsF5(obj.getAngle1()))
			ss += "setAngle1(" + g.a.b + "), ";
		if (!obj.getAngleMode().getClass().getSimpleName().equals(
				((Functions) g.a.c).getFunction().getClass().getSimpleName()))
			ss += "setAngleMode(" + ((Functions) g.a.c).toXL() + "), ";

		// Trans
		if (!((FLOAT) (f.a.a)).equalsF5(obj.getTrans2()))
			ss += "setTrans2(" + f.a.a + "), ";
		if (!((FLOAT) (f.a.b)).equalsF5(obj.getTrans1()))
			ss += "setTrans1(" + f.a.b + "), ";
		if (!obj.getTransMode().getClass().getSimpleName().equals(
				((Functions) f.a.c).getFunction().getClass().getSimpleName()))
			ss += "setTransMode(" + ((Functions) f.a.c).toXL() + "), ";

		// Scale
		if (!((FLOAT) (e.a.a)).equalsF5(obj.getScale2()))
			ss += "setScale2(" + e.a.a + "), ";
		if (!((FLOAT) (e.a.b)).equalsF5(obj.getScale1()))
			ss += "setScale1(" + e.a.b + "), ";
		if (!obj.getScaleMode().getClass().getSimpleName().equals(
				((Functions) e.a.c).getFunction().getClass().getSimpleName()))
			ss += "setScaleMode(" + ((Functions) e.a.c).toXL() + "), ";

		// Influence
		if (!((FLOAT) (c.a.a.a)).equalsF5(obj.getInfluence()))
			ss += "setInfluence(" + c.a.a.a + "), ";

		// farbe setzen
		if (use_GFXColor) ss += color;
		
		// letzte komma entfernen
		if (ss.lastIndexOf(',') > 0) {
			ss = ss.substring(0, ss.length() - 2);
		}
		return "PhiBall().(" + ss + ")";
	}

	private Node toGraph() {
		PhiBall obj = new PhiBall();
		obj.setName(name);
		obj.setNumber(((FLOAT) a.a.a.a).getValue());
		obj.setRadius(((FLOAT) b.a.a.a).getValue());
		obj.setFan2(((FLOAT) d.a.a).getValue());
		obj.setFan1(((FLOAT) d.a.b).getValue());
		obj.setFanMode(((Functions) d.a.c).getFunction());
		obj.setAngle2(((FLOAT) g.a.a).getValue());
		obj.setAngle1(((FLOAT) g.a.b).getValue());
		obj.setAngleMode(((Functions) g.a.c).getFunction());
		obj.setTrans2(((FLOAT) f.a.a).getValue());
		obj.setTrans1(((FLOAT) f.a.b).getValue());
		obj.setTransMode(((Functions) f.a.c).getFunction());
		obj.setScale2(((FLOAT) e.a.a).getValue());
		obj.setScale1(((FLOAT) e.a.b).getValue());
		obj.setScaleMode(((Functions) e.a.c).getFunction());
		obj.setInfluence(((FLOAT) c.a.a.a).getValue());
		
		if (phong!=null && aktTexture!=null) phong.setDiffuse(aktTexture);
		if (use_GFXColor) obj.setMaterial(phong);
		return obj;
	}

	@Override
	public String toString() {
		return "PhiBall {\n" + a.toString() + "\n" + b.toString() + "\n"
				+ c.toString() + "\n" + d.toString() + "\n" + e.toString()
				+ "\n" + f.toString() + "\n" + g.toString() + "\n" + "}\n";
	}
}

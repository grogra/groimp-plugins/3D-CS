/*
 * Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.blocks.xFrogFileParser;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Vector;

import javax.vecmath.Matrix4d;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.shading.Phong;
import de.grogra.math.ChannelMap;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ReaderSource;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Workbench;
import de.grogra.vfs.LocalFileSystem;

public class XFrogProjectLoader extends FilterBase implements ObjectSource,
	Workbench.Loader
{
	public XFrogProjectLoader (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (IOFlavor.PROJECT_LOADER);
	}
	
	public Object getObject ()
	{
		return this;
	}	
	
	private parser p1 = null;
	de.grogra.imp3d.Camera cam;

	public void loadRegistry (Registry registry) throws IOException
	{
		Reader input = ((ReaderSource) source).getReader();
		p1 = new parser();
		File f = ((FileSource) source).getInputFile ();
		registry.initFileSystem (new LocalFileSystem (IO.PROJECT_FS, f.getParentFile ()));		
		p1.parseFile(input, f.getParentFile (), registry);
		registry.setEmptyGraph ();

 		Tuple3f camera = p1.getCameraParams();
 		// camera ::= x=shiftRight, y=shiftUp, z=zoom
		Matrix4d mat = new Matrix4d();
		mat.rotX (-0.5 * Math.PI);
		mat.setTranslation(new Vector3d(camera.x, camera.y, 0));
//		mat.setScale(camera.z);
		cam = new de.grogra.imp3d.Camera ();
 		cam.setTransformation(mat);
 		de.grogra.imp3d.PerspectiveProjection proj = new de.grogra.imp3d.PerspectiveProjection ();
 		proj.setFieldOfView (camera.z * (float) Math.PI / 180);
 		cam.setProjection(proj);
		
 		Vector textures = p1.getTextures();
 		int i = 0;
 		for (Enumeration e = textures.elements() ; e.hasMoreElements() ;) {
 			ChannelMap cm = (ChannelMap) e.nextElement();
			Phong p = new Phong();
			p.setDiffuse(cm);
			registry.addSharedObject ("/project/objects/3d/materials", p, "texture" +i, true);			
			i++;
		}	 		
	}

	public void loadGraph (Registry r) throws IOException
	{
		((Node) r.getProjectGraph ().getRoot (Graph.MAIN_GRAPH))
			.appendBranchNode (p1.toGraph());
	}

	public void loadWorkbench (Workbench wb)
	{
		wb.setProperty (de.grogra.imp3d.View3D.INITIAL_CAMERA, null); // cam);
	}

}

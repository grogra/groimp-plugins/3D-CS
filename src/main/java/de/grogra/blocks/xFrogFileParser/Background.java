/*
* Copyright (C) 2002 - 2006 Lehrstuhl Grafische Systeme, BTU Cottbus
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package de.grogra.blocks.xFrogFileParser;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Sky;
import de.grogra.imp3d.shading.RGBAShader;

public class Background extends Expr {

	private final String l1Import = "import de.grogra.imp3d.objects.Sky;";
	private final String l2Import = "import de.grogra.imp3d.shading.RGBAShader;";

	public Background(Expr a, Expr b, Expr c) {
		this.a = a;
		this.b = b;
		this.c = c;
		if (aktKeyFrame == 0) {
			lightsGraphNodes.put(a.toString(), toGraph());
			sky = toXL();
			if (!imports.contains(l1Import)) imports.add(l1Import);
			if (!imports.contains(l2Import)) imports.add(l2Import);
		}
		if (debug)
			System.out.println(getClass().getSimpleName() + " :: " + a);
	}

	private String toXL() {
		return "[Sky.(setShader(new RGBAShader(" + ((INT) a).getValue() / 256.0
				+ "," + ((INT) b).getValue() / 256.0 + ","
				+ ((INT) c).getValue() / 256.0 + ")))]\n";
	}

	private Node toGraph() {
		Sky cc = new Sky();
		cc.setShader(new RGBAShader((float) (((INT) a).getValue() / 256.0),
				(float) (((INT) b).getValue() / 256.0), (float) (((INT) c)
						.getValue() / 256.0)));
		return cc;
	}

	@Override
	public String toString() {
		return "Background " + a.toString() + ", " + b.toString() + ", "
				+ c.toString();
	}

}

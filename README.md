# 3D-CS plugin 

3D-CS contains a construction set similar to Xfrog which was designed as part of a Diploma thesis. The plugin enables the creation of a project based on a xFrog file.

It adds the support of file type:
- ".xfr" (XFrog file)
